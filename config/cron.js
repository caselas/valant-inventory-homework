module.exports.cron = {
  expireItemsJob: {
    schedule: '1 * * * * *',
    onTick: function () {
	  sails.log.verbose('Checking if an item is expired every minute');
	  Inventory.destroyExpiredItems(function(err,result){
		if(err){
			sails.log.error("There was an error while destroying expired items.")
		}
		if (result.length>0){
			sails.log.info("sucesfully destroyed expired items");

			var opts={
				mandrillApiKey: sails.config.notification.mandrillApiKey,
				to: sails.config.notification.notification_email
			}

			var expiredIds=[];
			result.forEach(function(expired){
				expiredIds.push(expired.id);
			});

			opts.body="The elements with ids: "+expiredIds.join(",")+ " have expired";

			NotificationService.sendEmail(opts,function(err,notificationResult){
				if(err){
					sails.log.error("There was an error sending the email notification about the expired items");
				}
				sails.log("Successfully sent an email notification about the expired items");
			});
		}
		else{
			sails.log.info("There were no expired items");
		}
		
	  })
      

    }
  }
};