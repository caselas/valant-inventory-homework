# Valant Inventory Homework


## Requirements by valant
1. Add an item to the inventory
> When I add a new item to the inventory
>Then inventory contains information about the newly added item, such as >Label and Expiration Date.


2. Take an item from the inventory by Label
>When I took an item out from the inventory
>Then the item is no longer in the inventory

3. Notification that an item has been taken out.
>When I took an item out from the inventory
>Then there is a notification that an item has been taken out.


4. Notification that an item has expired.
>When an item expires
>Then there is a notification about the expired item.

## My take on this

Since this is just a test and not the real world, I will do a very basic model. In this case, I chose an Inventory model that will have a label, a description and an expirationDate, instead of having a Product model that relates to Inventory where I would have to write only once the label and the description for one product. I do it like this so I don't have to do any JOINS while searching for a particular item.

Since I want to keep account of the items that have been taken out of the Inventory I chose a Prescription model where I will keep the same info I had in the Inventory model (no auto primary key here, I will keep the Iventory ID) and add a new field prescriptionDate to keep account of when the transaction was done.

For the notification system I assume we are going to send an emails. To send the notification you will to use the url param "email=true".

For the batch, I would probably do a cron job in the real world (if the program runs on Linux) but since it will be easier to use a cron-like node.js module for this test, I will do so.

## Architecture

I chose Node.JS and sails.js as the MVC framework since it's a very simple framework and will be able to serve my needs for this exercise.

I won't use a DB, just the local-disk adapter from sails so you don't have to set up the DB.

For notifications, I assume we want to send emails and found a simple way to do it with the [mandrill service](https://mandrillapp.com). To make it more simple to integrate the service I use the [nodemailer](https://github.com/andris9/Nodemailer) library and its [nodemailer-mandrill-transport](https://github.com/rebelmail/nodemailer-mandrill-transport).

**For the notification functionallity to work you will need a mandrill API key. I will provide it via mail since I don't want it to be public**

For running the program you will need to install:

* [Git](https://git-scm.com)
* [Node.js](https://nodejs.org) 
* [Sails.js](https://sailsjs.org) sudo npm install -g sails
* [Mocha](http://mochajs.org/) npm install -g mocha
 
## Configuration
* Mandrill API Key (you can use the one I provided or create a new one)
* email to send to (by default it will be my Apple's mail address)

Once all of them are installed clone the repo:

```shell
git clone https://github.com/pablocaselas/valant-inventory
cd valant-inventory
npm install
```

## Up and running

```
sails lift
```

## Testing
```
npm test
```


This should run the program on port 1337, so make sure nothing else is using that port.

## Explaining where everything is

1. Models: ```api/models/```
2. Controllers: ```api/controllers/```
3. Services: ```api/services/```
4. Routes: ```config/routes.js```
5. Batch: ```config/cron.js```

## Getting it done

* Add an item to the inventory

For this purpose I created an inventory Model under ```api/models/Inventory.js``` where I added the following attributes: label (required), expirationDate (required) and description (optional). I let this model have auto primary key since it's not important for this test.

I also created an Inventory Controller under ```api/controllers/InventoryController.js``` where I have an add method that will be matched to  ```HTTP POST /inventory ``` where you will be able to add new items to the inventory passing the object in the request body. This is configured under ```config/routes.js```

* Take an item from the inventory

For this purpose I created a prescription Model, since I would like to keep account of the meds that have been taken out for medical reasons. When an item from the inventory expires I will just destroy it and won't keep a record.

I created the prescribe method in the Prescription Controller that will handle ``` HTTP POST /prescribe/:label ``` where ':label' is the label of the medication you want to take out.

I also created a Model method to take care of the implementation details of the prescription (it will find the closest to expiration medication out of the inventory and record the prescription).

* Notification that an item has been taken out of the inventory.

In the InventoryController prescribe method I will use NotificationService to send an email when the prescription has been recorded.

* Notification that an item has expired

To address this, I will use sails-hook-cron module to run it every day (but for better testing I will make it run every minute).
In this batch I will find the itemes that have expired, destroy them and send an email with the NotificationService.