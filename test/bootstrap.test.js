var Sails = require('sails'),
  sails;

before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift.
  this.timeout(5000);

  Sails.lift({
    // configuration for testing purposes
  }, function(err, server) {
    sails = server;
    if (err) return done(err);
    // here you can load fixtures, etc.

	Inventory.create({"label":"ibuprofen", expirationDate: new Date("2015-11-21T12:18:42.518Z")}).exec(console.log);
	Inventory.create({"label":"aspirin", expirationDate: new Date("2015-11-21T12:18:42.518Z")}).exec(console.log);
	
    done(err, sails);
  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  Sails.lower(done);
});