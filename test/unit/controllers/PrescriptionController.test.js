var request = require('supertest');

describe('PrescriptionController', function() {

  describe('#prescribe()', function() {
    it('should create a new prescription and take out one (not expired) ibuprofen from the inventory', function (done) {
      request(sails.hooks.http.app)
        .post('/prescribe/ibuprofen?email=false')
        .send()
        .expect(200,done);
    });
	
	it('should fail to prescribe because there is no item in the inventory', function (done) {
      request(sails.hooks.http.app)
      .post('/prescribe/failingLabel?email=false')
       .send()
       .expect(500,done);
    });
	
  });

});