var request = require('supertest');

describe('InventoryController', function() {

  describe('#add()', function() {
    it('should create a new item in the inventory', function (done) {
      request(sails.hooks.http.app)
        .post('/inventory')
        .send({ label: 'ibuprofen', description: 'just a test', expirationDate: new Date() })
        .expect(200,done);
    });

	it('should fail to create a new item in the inventory', function (done) {
      request(sails.hooks.http.app)
      .post('/inventory')
       .send({ label: 'ibuprofen', description: 'just a test' })
       .expect(400,done);
    });
  });

});