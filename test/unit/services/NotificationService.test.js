var assert = require('assert');

describe('NotificationService', function() {
  describe('#sendEmail()', function() {
    it('should send an email to the tester', function (done) {
		var opts={
			to: "pablo.caselas@me.com",
			mandrillApiKey: sails.config.notification.mandrillApiKey
		};
		NotificationService.sendEmail(opts, function(err,result){
			assert.equal(1,result.accepted.length);
			done();
		});
    });
	
	it('should fail send an email to the tester due to no email sender or api key', function (done) {
		var opts={
			to: "sdfasdfa",
			mandrillApiKey: sails.config.notification.mandrillApiKey
		};
		
		NotificationService.sendEmail(opts, function(err,result){
			assert.equal(undefined,result.messageId);
			done();
		});
    });

  });
});