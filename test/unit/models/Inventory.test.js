var assert = require('assert');

describe('InventoryModel', function() {
  describe('#create()', function() {
    it('should create a new inventory item', function (done) {
	 var inventoryItem={label:"ibuprofen", expirationDate: new Date("2015-10-31T11:19:49.663Z")};
      Inventory.create(inventoryItem)
        .then(function(results) {
			assert.equal(results.label, "ibuprofen");
          done();
        })
        .catch(done);
    });
  });

 describe('#destroyExpiredItems()', function() {
    it('should destroy all expired item', function (done) {
      Inventory.destroyExpiredItems(function(err, result){
		assert.notEqual(0, result.length);
		done()
	  })
    });
  })

});