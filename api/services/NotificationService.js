'use strict';
var nodemailer = require('nodemailer');

var mandrillTransport = require('nodemailer-mandrill-transport');

module.exports = {

    sendEmail: function(options, cb) {	
 		
		var transport = nodemailer.createTransport(mandrillTransport({
			auth: {
				apiKey: options.mandrillApiKey
			}
		}));
 
		transport.sendMail({
			from:options.from||"pablocaselas@gmail.com",
	    	to: options.to || 'pablo.caselas@me.com',
	    	subject: options.subject || 'Notification',
	    	html: options.body || '<p>How are you?</p>'	
		}, function(err, info) {
	  		if (err) {
	    		console.error(err);
				cb(err);
	  		} else {
	    		console.log(info);
				cb(null,info);
	  		}
		});
	}
}