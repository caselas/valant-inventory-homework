/**
 * PrescriptionController
 *
 * @description :: Server-side logic for managing Prescriptions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	prescribe:function(req,res){
		/*
		The following should be done with transactions but since I'm not doing it with a proper DB
		I figured I just do them manually
		*/
		var label=req.param("label");
		//sails doesn't parse url params so I'm converting the string email paramter from string to boolean
		var email=req.param("email")=='true'?true:false;
		sails.log.verbose("prescribing by label:",label);
		if (!label) return res.send(400);
		Prescription.prescribeByLabel({"label":label}, function(err, result){
			if (err){
				sails.log.error("error prescribing label",err);
				return res.send(500);
			}
			
			if (email){
				//send notification and return the response
				var opts={
					mandrillApiKey: sails.config.notification.mandrillApiKey,
					to: sails.config.notification.notification_email
				};
				opts.body="The "+result[0].label+ " with id "+result[0].id+ " has been prescribed";
				
				NotificationService.sendEmail(opts,function(err,notificationResult){
					if (err){
						sails.log.error("There was an error processing the notification",err);
						return res.send(500);
					}
					sails.log.info("Succesfully prescribed the medication and sent the notification");
					return res.send(200,result);
				})
			}
			else{
				//just return the response
				sails.log.info("Succesfully prescribed the medication");
				return res.send(200,result);
			}
		})
	}
};

