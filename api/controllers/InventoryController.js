/**
 * InventoryController
 *
 * @description :: Server-side logic for managing Inventories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	/*
	* It will create a new inventory item from the request body
	*/
	add: function(req,res){

		sails.log("received a petition to create a new item in the inventory: ",req.body);
		//sanity check
		if (!req.body) return res.send(400);
		if (!req.body.label || !req.body.expirationDate) return res.send(400);
		
		Inventory.create(req.body).exec(function(err,result){
			if(err) {
				sails.log.error("Error creating a new item in the inventory",err);
				return res.send(500);
			}
			sails.log.info("Successfully created a new item in the inventory with id:",result.id);
			return res.send(200, result);
		});
	}
};

