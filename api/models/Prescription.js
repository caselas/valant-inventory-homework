/**
* Prescription.js
*
* @description :: Simple model with the same attributes the inventory model has
*				  plus a prescriptionDate. Primary key will be the same the inventory had,
*				  so it's called id, it's a number and is unique
*				  
*				  This model will also have a class method that will help make the controller easier to read
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  autoPK:false,
  attributes: {
	id:{
		type:'integer',
		primaryKey:true,
		unique:true
	},
	label:{
		type:"string",
		required:true
	},
	description:{
		type:"string"
	},
	prescriptionDate:{
		type:"date",
		required:true
	}
  },
  prescribeByLabel: function(opts, cb){
	sails.log.verbose("opts:",opts);
	
	if (!opts) return cb({"err":"options must be an object with a label property"});
	if (!opts.label) return cb({"err":"options must be an object with a label property"});
	
	var whereClause={where: {label:opts.label, expirationDate:{">":new Date()}}, sort:'expirationDate ASC'};
	
	Inventory.findOne(whereClause).exec(function(err,inventoryResult){
		if(err){
			sails.log.error("error finding item in inventory for prescription",err);
			return cb(err);
		}
		
		if (!inventoryResult) return cb({"err":"couldn't find the item in the inventory"});
		
		sails.log.verbose("Found an item for prescription",inventoryResult);
		
		var prescription={
			id: Number(inventoryResult.id),
			label: inventoryResult.label,
			description: inventoryResult.description,
			prescriptionDate: new Date(),
			expirationDate: new Date(inventoryResult.expirationDate)
		};
		
		sails.log("Going to create prescription ",prescription);
		
		Prescription.create(prescription).exec(function(err,prescriptionResult){
			if (err){
				sails.log.error("error saving prescription",err, prescription);
				return cb(err);
			}
			sails.log.verbose("Succesfully created prescription a prescription with id:", prescriptionResult.id);
			sails.log.verbose("Going to destroy the item from the inventory");
			
			inventoryResult.destroy(function(err,result){
				if (err){
					sails.log.error("error destroying inventory item",err);
					return cb({"err":"error destroying inventory item"});
				}
				return cb(null, result);
			});
		})
	});
  }
};

