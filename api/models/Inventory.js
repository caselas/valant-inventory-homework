/**
* Inventory.js
*
* @description :: Simple inventory model with auto primary key, label and expirationDate as
* 				  required attributes and description as optional
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
	label:{
		type:"string",
		required:true
	},
	description:{
		type:"string"
	},
	expirationDate:{
		type:"date",
		required:true
	}
  },
  destroyExpiredItems: function(cb){
	var whereClause={expirationDate:{"<":new Date()}};
	Inventory.destroy(whereClause, function(err,result){
		if (err){
			sails.log.error("error destroying expired items");
			return cb(err);
		}
		if(result.length>0) sails.log.info("Succesfully destroyed expired items");
		return cb(null,result);
	})
  }
  
};

